The code of this app is licensed under GPLv3, based on the code of Cyanogenmod Theme Engine Template (Apache v2).

The artwork are licensed under Creative Commons Attribution-NonComercial-ShareAlike 4.0 International except for the material released under another licenses.

**Icon Licenses**

xphnx (BY-NC-SA 4.0): jadcompass, compass, communitycompass, nicecompass, gadgetbridge, opentraining, budget, androidrun, tinfoilfb, tinfoiltw, lockclock, quranandroid, synapse, prefmanager

Google (CC BY-SA 4.0 International): apps, alarm, browser, calendar, camera1, camera2, cellbroadcast, clock, connectbot, contacts, davdroid, downloads, fennec, firefox, filemanager, firewall, gallery, help, mail, lkeyboard, osmand, owncloud, phone, settings, sms, soundrecorder, xbmc, myownotes, owncloudnewsreader, otp, ebook, cipsimple, wppolice, satstat, adaway, opencamera, chatsecure, droidwall, kdeconnect, deskcon, linconnect, mythdroid, mythmote, keepassdroid, passandroid, pftpd, ppsspp, andstatus, mustard, impeller, diasporawebclient, friendica, twidere, mustardmod, systemappmover, orweb, tigase, conversations, beem, bombusmod, xabber, yaxim, amaze, fileexplorer, smsbackup, filemanagerpro, kerneladiutor, palemoon, icecat, andbible, ask, csipsimple, dictionaryformids, dsub, flym, ghostcommander, 
hackerskeyboard, justplayer, kerneladiutor, lkeyboard, myownnotes, netmbuddy, nlpconfig, orweb, osmonitor, owncloudnewsreader, owncloudsms, performancecontrol, pftpd, simplealarm, systemappmover, documents, xbmc, smssecure, textsecure, lightningbrowser, tintbrowser, ftpserver, androsens, vxconnectbot, irssiconnect, webopac, quickdic, gappsbrowser, otaupdate, bookmarkff, bookmarkbr, apktrack, getbackgps

Cyanogenmod (Apache v2): audiofx, themes, onze, twelf, trebuchet

Austin Andrews @templarian (SIL Open Font License 1.1): calculator, wikipedia, bitcoin, bitconium, coinbase, greenaddress, greenbits, testnet3, documentviewer, opendocumentreader, apg, openkeychain, apvpdfreader, mupdf, pdfreader, vudroid, orbot, orwall, barcodescanner, systemappmover, apktrack

William Theaker/Robert Martinez(CC BY-SA 3.0): fdroid

Gamma-aspirin (GFDL): music

Telegram LLC (GPLv3): telegram

Wordpress (GPL): wordpress

Wikipedia (CC BY-SA 3.0): wikipedia

Angelus (CC BY-SA 3.0): bitmask

Ocal (Public Domain): adaway, chess, chesswalk, droidfish

Gabriel @lastrosestudios (SIL Open Font License 1.1): smsbackup, sismicsreader, feedhive, ttrss, sparserss, ttrss, simplerss, newsblur, antennapod, carcast, podax

Sun Microsystems (Apache v2): opendocumentreader

Mysitemyway (Public Domain): alogcat, alogcatroot, catlog

GNU (GPLv3) andstatus, mustard, mustardmod

Friendica (AGPL): friendica

Twidere/lordfriend (GPLv3): twidere

Delapouite (CC BY 3.0): trebuchet

Xmpp Standards (MIT): tigase, conversations, beem, bombusmod, xabber, yaxim

Syncthing (MPLv2): syncthing

Larry Ewing, Simon Budig, Anja Gerwinski (Attribution) : kerneladiutor

OpenVPN Technologies (CC BY-SA 3.0): openvpn, openvpnsettings

Mumble (BSD): plumble

Rama (CC BY-SA 2.0 fr): subsonic

Identiconizer (Apache v2): identiconizer

VLC (GPLv3): vlc

Swiftnotes (Apache v2): swiftnotes

Keepass2android (GPLv2): keepass2android

Anki (GPLv3): ankidroid

Anymemo (GPLv2): anymemo


**Bootanimation**

William Theaker/Robert Martinez(CC BY-SA 3.0): F-droid Logo

