## TwelF CM12 Theme for FLOSS apps ##

This theme contains exclusively icons for the apps hosted on [F-droid FLOSS Repository] (https://f-droid.org/).

TwelF follows the guidelines of Google Material Design for Android Lollipop aiming to provide a unified and minimalistic look at devices using CM12 with only Libre or Open Source Apps. 

## Requirements ##

* A device running the Cyanogenmod 12 ROM (See the [list of compatible devices](https://gitlab.com/xphnx/twelf_cm12_theme/wikis/compatible-devices))
* [F-droid client](https://f-droid.org/) installed

For issues, comments or icon request, please [use the issue tracker] (https://gitlab.com/xphnx/twelf_cm12_theme/issues)

# Features #

* Icon Pack
* Bootanimation
* Wallpaper & Lockscreen 

[Changelog] (https://gitlab.com/xphnx/twelf_cm12_theme/blob/master/CHANGELOG.md)

Status: Alpha.

[![Get_it_on_F-Droid.svg](https://gitlab.com/uploads/xphnx/twelf_cm12_theme/a4649863bd/Get_it_on_F-Droid.svg.png)](https://f-droid.org/app/org.twelf.cmtheme)

Licenses [GPLv3](http://www.gnu.org/licenses/gpl-3.0.html) - [Creative Commons 4.0 BY-NC-SA] (http://creativecommons.org/licenses/by-nc-sa/4.0/) and other libre or open licenses (see [LICENSE](https://gitlab.com/xphnx/twelf_cm12_theme/blob/master/LICENSE.md) for more details).

# Snapshots #

![Screenshot12](https://gitlab.com/xphnx/twelf_cm12_theme/uploads/d87bee1f24b48660e6f8f364adadef11/Screenshot12.png)
