# v0.1 #

**Icons:**
alarm, apps, audiofx, broadcast, browser, calculator, calendar, camera, clock, connectbot, contacts, davdroid, downloads, fdroid, fennec, filemanager, firefox, firewall, gallery, help, k9mail, keiboard, mail, music, osmand, owncloud, phone, settings, simtk, telegram, twelf, vlc, voicerecorder, wikipedia, wordpress. 

# v0.2 #

**Fixed critical issue #2**

**Fixed icons:** mail, k9mail, soundrecorder, vlc, afw+

**New icons:** bitcoin, bitconium, coinbase, greenaddress, greenbits, testnet3, bitmask, hn, myownnotes, owncloudnewsreader, freeotp, cool reader, epub3reader, fb reader, page turner, xbmc, wifi privacy police, satstat, sms, csipsimple

**Bootanimation**

# v0.3 #

**New icons:** adaway, opencamera, chatsecure, documentviewer, opendocumentreader, droidwall, apg, openkeychain, kdeconnect, linconnect, alogcat, alogcatroot, catlog, mythdroid, mythmote, keeppassdroid, passandroid, apvpdfreader, mupdf, pdfreader, vudroid, pftpd, antennapod, carcast, podax, ppsspp, sismicsreader, feedhive, ttrss, sparserss, simplerss, newsblur, andstatus, diasporawebclient, friendica, impeller, mustard, mustardmod, systemappmover, themes, orbot, orwall, orweb, transdroid, transdrone, transdroidsearch, trebuchet, xdafeedreader, tigase, conversations, beem, bombusmod, xabber, yaxim, amaze, barcodescanner, smsbackup+, palemoon, icecat, fileexplorer, filemanagerpro, kerneladuitor


**Wallpaper and Lockscreen**

**Fix icon:** davdroid

# v0.4 #

**New icon for TwelF**

**New icons:** andbible, ask, communitycompass, compass, dictionaryformids, documents, dsub, flym, gadgetbridge, ghostcommander, github, hackerskeyboard, identiconizer, jadcompass, jupiterbroadcast, justplayer, lockclock, netmbuddy, nicecompass, nlpconfig, openvpn , openvpnsettings, osmonitor, osmonitor, plumble, redreader, simplealarm, subsonic, syncthing, terminal, terminalemulator

**Fix icons:** keyboard, wikipedia

**Remake icons:** apps, csipsimple , kerneladiutor, myownnotes, orbot, orwall, orweb, owncloudnewsreader, owncloudsms, performancecontrol, pftpd, systemappmover, telegram, xbmc

# v0.5 #

**Fix icons:** simtk, clocklock, openvpn, terminalemulator, unifiednlp, github

# v0.6 #

**New icons:** smssecure, textsecure, opentraining, budget, androidrun, droidfish, chess, chesswalk, lightningbrowser, tintbrowser, ftpserver, androisens, tinfoilfb, tinfoiltw, vxconnectbot, irssiconnectbot

**Remake icons:** lockclock, kerneladiutor, myownnotes, nlpconfig, owncloudnews, owncloudsms, redreader, systemappmover, terminal, terminalemulator

# v0.7 #

**New icons:** swiftnotes, quickdic, gappsbrowser, webopac, quranandroid, ankidroid, openimgur, anymemo, keepass2android, synapse, otaupdate, bookmarkbr

**Remake icons:** fdroid, owncloudnews, lockclock, trebuchet

# Next update v0.8 #

**New icons:** getbackgps, apktrack, prefmanager

# TODO # 

**Shipping svgs instead pngs** thanks to t184256

